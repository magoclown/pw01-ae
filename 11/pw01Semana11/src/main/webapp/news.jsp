<%-- 
    Document   : news
    Created on : 24/11/2020, 07:32:41 PM
    Author     : magoc
--%>
<%@page import="java.util.List"%>
<%@page import="com.pw.pw01semana11.models.Comment"%>
<%@page import="com.pw.pw01semana11.models.New"%>
<%
    New element = (New) request.getAttribute("New");
    List<Comment> comments = (List<Comment>) request.getAttribute("Comments");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="assets/css/main.css">
    </head>
    <body>
        <jsp:include page="navbar.jsp" />  
        <div class="container">
            <div class="row">
                <h1 class="col-12"><%= element.getTitle()%></h1>
                <small class="text-muted col-12">Categoria <%= element.getCategory().getName()%></small>

                <div id="carouselExampleControls" class="carousel slide col-12" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="<%= element.getPathImage()%>" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <p class="col-12">
                    <%= element.getDescription()%>
                </p>
            </div>
            <div class="row">
                <div class="col-12" id="comment-container">
                    <h3>Comentarios</h3>
                    <form class="col-12" method="POST" action="CommentsController">
                        <div class="from-group">
                            <textarea class="form-control" name="content"></textarea>
                            <input type="hidden" name="idNews" value="<%= element.getId()%>">
                            <input type="hidden" name="parent" value="-1">
                        </div>
                        <div class="from-group">
                            <input type="submit" class="btn btn-success" value="Comentar">
                        </div>
                    </form>
                    <%
                        for (Comment comment : comments) {
                    %>
                    <div class="media"> 
                        <a class="delete btn btn-danger" href="DeleteCommentController?idNews=<%= element.getId()%>&id=<%= comment.getId()%>">Eliminar</a>
                        <img src="https://cdnb.artstation.com/p/assets/images/images/032/331/289/large/hitesh-chauhan-screenshot001.jpg?1606145106" class="mr-3" alt="...">
                        <div class="media-body">
                            <h5 class="mt-0"><%= comment.getUser().getUsername()%></h5>
                            <%= comment.getContent()%>

                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>
    </body>
</html>
