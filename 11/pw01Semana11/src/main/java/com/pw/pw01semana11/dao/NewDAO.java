/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01semana11.dao;

import com.pw.pw01semana11.models.Category;
import com.pw.pw01semana11.models.New;
import com.pw.pw01semana11.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author magoc
 */
public class NewDAO {
//    CREATE TABLE `pw01`.`new` (
//  `idnew` INT NOT NULL AUTO_INCREMENT,
//  `title` VARCHAR(60) NULL,
//  `description` TEXT NULL,
//  `id_category` INT NULL,
//  `path_image` VARCHAR(120) NULL,
//  PRIMARY KEY (`idnew`),
//  UNIQUE INDEX `idnew_UNIQUE` (`idnew` ASC) VISIBLE,
//  INDEX `fk_cateogry_idx` (`id_category` ASC) VISIBLE,
//  CONSTRAINT `fk_cateogry`
//    FOREIGN KEY (`id_category`)
//    REFERENCES `pw01`.`category` (`idcategory`)
//    ON DELETE NO ACTION
//    ON UPDATE NO ACTION);

//    USE `pw01`;
//    DROP procedure IF EXISTS `InsertNew`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `InsertNew` (
//    IN `pTitle` varchar(60),
//    IN `pDescription` text,
//    IN `pId_category` int,
//    IN `pPath_image` varchar(120)
//    )
//    BEGIN
//    INSERT INTO `new`
//    (`title`,
//    `description`,
//    `id_category`,
//    `path_image`)
//    VALUES
//    (pTitle,
//    pDescription,
//    pId_category,
//    pPath_image);
//    END$$
//
//    DELIMITER ;
//
    public static int insertNew(New nNew) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "call InsertNew(?,?,?,?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, nNew.getTitle());
            statement.setString(2, nNew.getDescription());
            statement.setInt(3, nNew.getCategory().getId());
            statement.setString(4, nNew.getPathImage());
            return statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }
//    USE `pw01`;
//    DROP procedure IF EXISTS `GetNews`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `GetNews` ()
//    BEGIN
//    SELECT `new`.`idnew`,
//        `new`.`title`,
//        `new`.`description`,
//        `new`.`id_category`,
//        `new`.`path_image`
//    FROM `new`;
//
//    END$$
//
//    DELIMITER ;
//

    public static List<New> getNews() throws SQLException {
        List<New> news = new ArrayList<>();
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "call GetNews()";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int id = result.getInt(1);
                String title = result.getString("title");
                String description = result.getString("description");
                int idCategory = result.getInt("id_category");
                String pathImage = result.getString("path_image");
                Category category = CategoryDAO.getCategory(idCategory);
                news.add(new New(id, title, description, category, pathImage));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return news;
    }

//    USE `pw01`;
//DROP procedure IF EXISTS `GetNew`;
//
//DELIMITER $$
//USE `pw01`$$
//CREATE PROCEDURE `GetNew` (
//IN `pIdnew` int 
//)
//BEGIN
//
//SELECT `new`.`idnew`,
//    `new`.`title`,
//    `new`.`description`,
//    `new`.`id_category`,
//    `new`.`path_image`
//FROM `new`
//WHERE `new`.`idnew` = pIdnew;
//
//END$$
//
//DELIMITER ;
//

    public static New getNews(int idNews) {
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "call GetNew(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, idNews);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int id = result.getInt(1);
                String title = result.getString("title");
                String description = result.getString("description");
                int idCategory = result.getInt("id_category");
                String pathImage = result.getString("path_image");
                Category category = CategoryDAO.getCategory(idCategory);
                return new New(id, title, description, category, pathImage);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        return null;
    }
}
