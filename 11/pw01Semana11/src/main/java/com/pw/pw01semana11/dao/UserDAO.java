/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01semana11.dao;

import com.pw.pw01semana11.models.User;
import com.pw.pw01semana11.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author magoc
 */
public class UserDAO {

//    CREATE TABLE `pw01`.`user` (
//  `iduser` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//  `username` VARCHAR(45) NULL,
//  `password` VARCHAR(100) NULL,
//  PRIMARY KEY (`iduser`),
//  UNIQUE INDEX `iduser_UNIQUE` (`iduser` ASC) VISIBLE,
//  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE);
//    USE `pw01`;
//    DROP procedure IF EXISTS `InsertUser`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `InsertUser` (
//    IN `pUsername` varchar(45),
//    IN `pPassword` varchar(100)
//    )
//    BEGIN
//    INSERT INTO user
//    (`username`,
//    `password`)
//    VALUES
//    (pUsername,
//    pPassword);
//
//    END$$
//
//    DELIMITER ;
//
    public static int insertUser(User user) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "call InsertUser(?,?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            return statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

//    USE `pw01`;
//    DROP procedure IF EXISTS `LogInUser`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `LogInUser` (
//    IN `pUsername` varchar(45),
//    IN `pPassword` varchar(100)
//    )
//    BEGIN
//    SELECT u.iduser as ID,
//        u.username
//    FROM user u
//    WHERE u.username = pUsername
//    AND u.password = pPassword;
//
//    END$$
//
//    DELIMITER ;
//

    public static User logInUser(User user) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "call LogInUser(?,?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                int id = resultSet.getInt("ID");
                String username = resultSet.getString("username");
                return new User(id, username);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
//    USE `pw01`;
//DROP procedure IF EXISTS `GetUser`;
//
//DELIMITER $$
//USE `pw01`$$
//CREATE PROCEDURE `GetUser` (
//IN `pIduser` int unsigned
//)
//BEGIN
//SELECT `user`.`iduser`,
//    `user`.`username`
//FROM `pw01`.`user`
//WHERE `user`.`iduser` = pIduser;
//
//END$$
//
//DELIMITER ;
//

    public static User getUser(int idUser) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "CALL GetUser(?);";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, idUser);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                int id = resultSet.getInt(1);
                String username = resultSet.getString(2);
                return new User(id, username);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

}
