/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw01semana06.models;

/**
 *
 * @author magoc
 */
public class CharacterController {

    // Constantes de jugablidad
    public static double PI = 3.141516;

    // Propierdades de Clase
    private String name;
    private double life;
    private boolean isDeath;

    public CharacterController() {
    }

    public CharacterController(String name, double life) {
        this.name = name;
        this.life = life;
    }

    public CharacterController(String name, double life, boolean isDeath) {
        this.name = name;
        this.life = life;
        this.isDeath = isDeath;
    }

    /**
     * Metodo para retornar el nombre del controlador
     *
     * @return Nombre del controlador
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLife() {
        return life;
    }

    public void setLife(double life) {
        this.life = life;
    }

    public boolean isIsDeath() {
        return isDeath;
    }

    public void setIsDeath(boolean isDeath) {
        this.isDeath = isDeath;
    }

    public void move() {
        System.out.println("Move CharacterController");
    }

    public void death() {
        this.isDeath = true;
    }

    @Override
    public String toString() {
        return "CharacterController{" + "name=" + name + ", life=" + life + ", isDeath=" + isDeath + '}';
    }

}
