/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw01semana06.models;

/**
 * Clase con la cual se controlara el movimiento de los jugadores
 * @author magoc
 */
public class PlayerController extends CharacterController {

    /**
     * Constructor de PlayerController
     */
    public PlayerController() {
    }

    /**
     * Constructor de PlayerController
     *
     * @param name Nombre de Controller
     * @param life Cantidad de vida del controller
     */
    public PlayerController(String name, double life) {
        super(name, life);
    }

    /**
     * Constructor de PlayerController
     * @param name Nombre de Controller
     * @param life Cantidad de vida del controller
     * @param isDeath Saber si el controller esta muerto
     */
    public PlayerController(String name, double life, boolean isDeath) {
        super(name, life, isDeath);
    }

    @Override
    public void move() {
        super.move(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Move PlayerController");
    }

}
