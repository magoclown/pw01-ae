// // Alcance Global
// var
// // Alcance de bloque
// let
// const
var hola = "Hola Mundo";

function holaMundo() {
    let numero = 5;
    let text = "5";
    if(text) {
        let imprimir = true;
    }
    if(imprimir) {

    }
    // Triple igualacion, compara valor Y TIPO
    if (numero === text) {
        
    }
}

function adiosMundo() {
    let adiosMundo = hola;
}

function greetings(name = 'Jose') {
    alert(`Hola ${name}`);
}
