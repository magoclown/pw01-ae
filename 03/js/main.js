function detectForm(e) {
  let formulariosReg = document.forms["registro"];
  let errorMessage = null;
  e.preventDefault();
  console.log(e);
  console.log(formulariosReg.password.value);

  if (formulariosReg.email.value.length === 0) {
    formulariosReg.email.classList.add("form-error");
    errorMessage += "Verifique Email";
    appendError("Verifique Email");
  } else {
    formulariosReg.email.classList.remove("form-error");
  }
  if (formulariosReg.password.value.length < 8) {
    formulariosReg.password.classList.add("form-error");
    errorMessage += "Verifique Password";
    appendError("Verifique Password");
  } else {
    formulariosReg.password.classList.remove("form-error");
  }
  // showError(true, errorMessage);
  displayError(errorMessage);
}
function showError(show, message) {
  let formularioRegistro = document.querySelector("div#error-message");
  if (show) {
    formularioRegistro.classList.remove("hide");
  } else {
    formularioRegistro.classList.add("hide");
  }
  formularioRegistro.append(message);
}

function appendError(message) {
  let formularioRegistro = document.querySelector("div#error-message");
  formularioRegistro.innerHTML += `${message}<br>`;
}
function displayError(show) {
  let formularioRegistro = document.querySelector("div#error-message");
  if (show) {
    formularioRegistro.classList.remove("hide");
  } else {
    formularioRegistro.classList.add("hide");
  }
}
// debugger;

window.onload = () => {
  let formularioRegistro = document.querySelector("form#registro");
  let formularios = document.forms;
  let formulariosReg = document.forms["registro"];
  formularioRegistro.addEventListener("submit", detectForm);
  console.log(formularioRegistro["nombre"]);
  console.log(formularios[0]);
  console.log(formulariosReg[0]);

  formularioRegistro["nombre"].value = "Jose";

  let nombre = formularioRegistro["nombre"]?.value;
  let edad = formularioRegistro["edad"]?.value;
  let correo = formularioRegistro["correo"]?.value;
  let password = formularioRegistro["password"]?.value;
  // debugger;
  // for(const element in formularioRegistro) {
  //   console.log(element);
  // }
};
