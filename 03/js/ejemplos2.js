function hola(callback) {
    console.log("Hola");
    callback();
  }
  
  function mundo() {
    console.log(" mundo");
  }
  
  hola(mundo);
  
  var suma = (a, b) => a + b;
  
  suma(5, 5);
  
  var req = new XMLHttpRequest();
  req.onreadystatechange = function () {
    if (this.status === 200 && this.readyState == 4) {
        console.log(this.responseText);
    }
  };
  req.open('GET','https://api.openbrewerydb.org/breweries');
  req.send();