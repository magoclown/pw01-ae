/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01semana09.controllers;

import com.pw.pw01semana09.models.Card;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author magoc
 */
@WebServlet(name = "MainPage", urlPatterns = {"/MainPage"})
public class MainPage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet MainPage</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet MainPage at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
        List<Card> cards = new ArrayList();
        cards.add(
                new Card(
                        "https://cdnb.artstation.com/p/assets/images/images/031/326/309/large/art-of-maki-sucubus-final.jpg?1603291612",
                        "Sucu",
                        "Quicky sucu",
                        "https://www.artstation.com/artwork/5XYqGw"
                ));
        cards.add(
                new Card(
                        "https://cdnb.artstation.com/p/assets/images/images/031/167/273/large/sean-raiko-tay-eky8-ciu4aeijir.jpg?1602788767",
                        "Odyssey Kha'Zix",
                        "Had the honor to illustrate the Odyssey Kha'Zix splash, was a fun piece to work on!~ as usual, mega thanks to my art director Mingchen Shen and the League Splash Team for their help and feedback that pushed it tremendously, couldn't have done it without them!\n"
                        + "In Collaboration with Riot Games",
                        "https://www.artstation.com/artwork/v2m2q3"
                ));
        request.setAttribute("Title", "Main Page");
        request.setAttribute("cards", cards);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
}
