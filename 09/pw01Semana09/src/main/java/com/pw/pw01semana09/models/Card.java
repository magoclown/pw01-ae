/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01semana09.models;

/**
 * Modelo de Card, el cual nos servira para mostrar Tarjetas
 * POJO
 * @author magoc
 */
public class Card {

    private String imgPath;
    private String title;
    private String description;
    private String cardLink;

    public Card() {
    }

    public Card(String imgPath, String title, String description, String cardLink) {
        this.imgPath = imgPath;
        this.title = title;
        this.description = description;
        this.cardLink = cardLink;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCardLink() {
        return cardLink;
    }

    public void setCardLink(String cardLink) {
        this.cardLink = cardLink;
    }

}
