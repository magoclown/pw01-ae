<%-- 
    Document   : index
    Created on : 27/10/2020, 07:55:36 PM
    Author     : magoc
--%>

<%@page import="com.pw.pw01semana09.models.Card"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    // No puede ser convertido de manera implicita
    // Requerimos convertirlo de manera explicita
    String title = (String) request.getAttribute("Title");
    List<Card> cards = (List<Card>) request.getAttribute("cards");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="MainPage">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <%
                    if (1 == 1) {
                %>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="User" aria-label="Search">
                    <input class="form-control mr-sm-2" type="password" placeholder="Password" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Login</button>
                </form>
                <%
                } else {
                %>
                <div class="my-2 my-lg-0">
                    <img width="30px" src="https://cdna.artstation.com/p/assets/images/images/031/138/110/large/david-holland-shot01-1080.jpg?1602707250"/>
                    <div>Jose</div>
                </div>
                <%
                    }
                %>
            </div>
        </nav>
        <div class="container">
            <h1><%= title%></h1>
            <div class="row">
                <h2 class="col-12">Lista</h2>
                <%
                    // For-each
                    // For-of
                    for (Card element : cards) {
                %>
                <div class="card col-3" >
                    <img src="<%= element.getImgPath() %>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><%= element.getTitle()%></h5>
                        <p class="card-text"><%= element.getDescription()%></p>
                        <a href="<%= element.getCardLink()%>" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <%
                    }
                %>
                <h2 class="col-12">For</h2>
                <%
                    for (int i = 0; i < 8; i++) {
                %>
                <!--<p class="col-3">Hola Mundo <b><%= i%></b></p>-->
                <div class="card col-3" >
                    <img src="https://cdnb.artstation.com/p/assets/images/images/030/510/179/large/betty-jiang-betty-concept-8.jpg?1600886955" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" target="_blank" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <%
                    }
                %>
                <h2 class="col-12">Table</h2>
                <table class="col-12 table">
                <tr>
                    <th>Titulo</th>
                    <th>Descripcion</th>
                    <th>Link</th>
                    <th>Estatus</th>
                    <th>Acciones</th>
                </tr>
                <%
                    for (Card card : cards) {
                %>
                <tr>
                    <td><%= card.getTitle()%></td>
                    <td><%= card.getDescription()%></td>
                    <td><%= card.getCardLink()%></td>
                    <td>Publicada</td>
                    <td><a class="btn btn-success">Editar</a><a class="btn btn-danger">Eliminar</a></td>
                </tr>
                <%
                    }
                %>
                <table>
            </div>
        </div>
    </body>
</html>
