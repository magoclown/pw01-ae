
class Shape {
    public height: number;
    public width: number;
    public area: number;
    public perimeter: number;

    public calculateArea(){

    }
}

class Circle extends Shape {
    public radio: number;

    public calculateArea(): number {
        return Math.PI * this.radio * this.radio;
    }
}

function main() {
    const circle = new Circle();
    circle.radio = 5;
    console.log(`Area: ${circle.calculateArea()}`);
}