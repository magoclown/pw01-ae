var formatBrewery = (name, state, city, web) => {
  return `<div class="element-brewery">
        <p>Nombre: ${name}</p>
        <p>Estado: ${state}</p>
        <p>Ciudad: ${city}</p>
        <p>Web: ${web}</p>
    </div>`;
};

function showTitle() {
  $("#title").show();
}

function hideTitle() {
  $("#title").hide();
}

function addTitle() {
  $("div.container").append('<h1 id="title">Titulo</h1>');
}

function removeTitle() {
  $("#title").remove();
}

function fetchAJAX() {
  console.log("CALL fetchAJAX");
  $.ajax({
    url: "https://api.openbrewerydb.org/breweries",
    method: "GET",
  }).done(function (data) {
    console.log("DATA FETCH");
    console.log(data);
    $.each(data, (k, v) => {
      console.log(`${k}: ${v}`);
      // $('div.ajax').append(v);
      $("div.ajax").append(
        formatBrewery(v.name, v.state, v.city, v.website_url)
      );
    });
    // $('div.ajax').append(data);
  });
}
var position = { x: 0, y: 0 };
$(function () {
  // Teclado
  $("body").keydown(function (data) {
    // console.log(`Handler for .keydown() called.`);
    // console.log(data);
    if (data.keyCode == 87) {
      position.y += 1;
    }
    if (data.keyCode == 83) {
      position.y -= 1;
    }
    if (data.keyCode == 68) {
      position.x += 1;
    }
    if (data.keyCode == 65) {
      position.x -= 1;
    }
    console.log(`Position X: ${position.x}, Position Y: ${position.y}`);
  });
});
