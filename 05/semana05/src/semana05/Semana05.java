/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana05;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author magoc
 */
public class Semana05 {

    public String name;
    Float average;
    protected Integer age;
    private Double size;

    public String[] Colors;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Semana05 ejemplo = new Semana05();
        ejemplo.average = 5.0f;
        ejemplo.printAverage();
        
//        OddCalculator calc = new OddCalculator();
        System.out.println(OddCalculator.isOdd(3));
        FibonacciCalculator.printFibonacciSequence(10);
        
        List<String> names = new ArrayList<String>();
        names.add("Jose");
        names.add("Maria");
        names.add("Aimee");
        names.add("Bryan");
        names.remove(0);
        for (String name : names) {
            System.out.println("Name: " + name);
        }
        
        Map<String, Integer> dictionary = new HashMap<String, Integer>();
        dictionary.put("Jose", 26);
        dictionary.put("Aimee", 25);
        dictionary.put("Bryan", 24);
        dictionary.put("Mario", 21);
        System.out.println("Persona: " + dictionary.get("Bryan"));
        for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
            Object key = entry.getKey();
            Object val = entry.getValue();
            System.out.println("Key: " + key + ", Val:" + val);
        }
    }

    public void printAverage() {
        System.out.println("Average: " + average.toString());
    }

}
