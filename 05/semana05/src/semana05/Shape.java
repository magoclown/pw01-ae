/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana05;

/**
 *
 * @author magoc
 */
public abstract class Shape {
    
    // Metodos abstractos son aquellos que son declarados
    // mas no definidos
    public abstract float area();
    public abstract float perimeter();
    
    public void printArea() {
        System.out.println(area());
    }
}
