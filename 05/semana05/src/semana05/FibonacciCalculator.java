/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana05;

/**
 *
 * @author magoc
 */
public class FibonacciCalculator {

    public static void printFibonacciSequence(int iteration) {
        String sequence = "";
        int a = 0;
        int b = 1;
        int sum = 0;
        for (int i = 2; i <= iteration; i++) {
            sum = a + b;
            sequence += b + ",";
            a = b;
            b = sum;
//            sequence += sum + ",";
        }
        System.out.println(sequence);
    }

}
