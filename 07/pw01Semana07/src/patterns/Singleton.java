/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

/**
 * Es un patron que nos permite tener una sola instancia en toda la existencia
 * @author magoc
 */
public class Singleton<T> {
    
    // Instancia estatica
    private static final Singleton instance;
    // Constructor estatico
    static {
        instance = new Singleton();
    }

    // Getter
    public static Singleton getInstance() {
        return instance;
    }    
}
