/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 * Enlista los niveles a los cuales podemos ir
 * @author magoc
 */
public enum Levels {
    Main_Screen,
    Tutorial,
    Credits,
    Level_Water,
    Level_Water_Boss,
    Game_Over,
    Thanks_For_Playing
}
