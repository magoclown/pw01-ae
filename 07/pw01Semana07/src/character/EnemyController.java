/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package character;

import Ejemplos.GameManager;

/**
 *
 * @author magoc
 */
public class EnemyController extends AIController {

    private int points;

    public EnemyController() {
    }

    public EnemyController(int points) {
        this.points = points;
    }

    public EnemyController(int points, boolean dead, float health) {
        super(dead, health);
        this.points = points;
    }

    @Override
    public void die() {
        super.die(); //To change body of generated methods, choose Tools | Templates.
        GameManager.getInstance().setScore(points);
    }

}
