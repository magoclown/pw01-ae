/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package character;

/**
 *
 * @author magoc
 */
public class PlayerController extends CharacterController {

    public PlayerController() {
    }

    public PlayerController(boolean dead, float health) {
        super(dead, health);
    }

    @Override
    public void move() {
        System.out.println("Move");
    }

    @Override
    public void attack() {
        System.out.println("Attack");
    }

}
