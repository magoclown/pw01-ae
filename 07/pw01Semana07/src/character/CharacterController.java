/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package character;

/**
 *
 * @author magoc
 */
public abstract class CharacterController {

    private boolean dead;
    private float health;

    public CharacterController() {
    }

    public CharacterController(boolean dead, float health) {
        this.dead = dead;
        this.health = health;
    }

    public abstract void move();

    public abstract void attack();

    public void takeDamage(float damage) {
        System.out.println("Character take " + damage + " damage");
        this.health -= damage;
        if (this.health <= 0) {
            die();
        }
    }

    public void die() {
        this.dead = true;
    }

    public double getHealth() {
        return health;
    }

    public boolean isDead() {
        return dead;
    }

}
