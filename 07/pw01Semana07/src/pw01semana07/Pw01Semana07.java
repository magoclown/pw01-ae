/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw01semana07;

import Ejemplos.GameManager;
import builder.Npc;
import builder.NpcBuilder;
import character.EnemyController;
import character.PlayerController;

/**
 *
 * @author magoc
 */
public class Pw01Semana07 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        GameManager.getInstance().initGame();
        System.out.println("Level: " + GameManager.getInstance().getLevelName());
        GameManager.getInstance().pressPlay();
        System.out.println("Level: " + GameManager.getInstance().getLevelName());
        GameManager.getInstance().loadWaterLevel();
        System.out.println("Level: " + GameManager.getInstance().getLevelName());

        PlayerController player = GameManager.getInstance().getPlayer();
        EnemyController firstEnemy = new EnemyController(10, false, 10);
        EnemyController secondEnemy = new EnemyController(20, false, 15);

        for (int i = 0; i < 10; i++) {
            player.move();
        }
        player.takeDamage(5);
        System.out.println("Player Health:" + player.getHealth());
        System.out.println("GM Player Health:" + GameManager.getInstance().getPlayer().getHealth());
        for (int i = 0; i < 2; i++) {
            player.attack();
        }
        firstEnemy.takeDamage(10);
        for (int i = 0; i < 20; i++) {
            player.move();
        }
        player.takeDamage(10);
        for (int i = 0; i < 2; i++) {
            player.attack();
        }
        secondEnemy.takeDamage(10);
        player.takeDamage(5);
        for (int i = 0; i < 2; i++) {
            player.attack();
        }
        secondEnemy.takeDamage(10);
        
        // Topar NPC
        NpcBuilder builder = new NpcBuilder();
        Npc npc = builder.withAge(27)
                .withName("Jose")
                .withMoney(100)
                .withRace("Human")
                .build();
        Npc npc2 = builder.withAge(27)
                .withName("Aimee")
                .withMoney(200)
                .withRace("Halfling")
                .build();
        Npc npc3 = builder.build();
        System.out.println("NPC: "  + npc);
        System.out.println("NPC: "  + npc.toString());
        System.out.println("NPC: "  + npc2);
        System.out.println("NPC: "  + npc3);
        npc2.setAge(28);
        System.out.println("NPC: "  + npc2);
        System.out.println("NPC: "  + npc3);
    }

}
