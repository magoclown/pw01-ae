/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplos;

import Utils.Levels;
import character.PlayerController;

/**
 *
 * @author magoc
 */
final public class GameManager {

    private static final GameManager instance;

    static {
        instance = new GameManager();
    }

    public GameManager() {
    }

    public static GameManager getInstance() {
        return instance;
    }

    private Levels levelName;
    private boolean isGameOver;
    private int score;
    private PlayerController player;

    public Levels getLevelName() {
        return levelName;
    }

    public void setLevelName(Levels levelName) {
        this.levelName = levelName;
    }

    public boolean isIsGameOver() {
        return isGameOver;
    }

    public void setIsGameOver(boolean isGameOver) {
        this.isGameOver = isGameOver;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score += score;
        System.out.println("Actual Score: " + this.score);
    }

    public PlayerController getPlayer() {
        return player;
    }

    public void setPlayer(PlayerController player) {
        this.player = player;
    }
    
    public void initGame() {
        levelName = Levels.Main_Screen;
        isGameOver = false;
        System.out.println("El juego se ha iniciado");
    }
    
    public void pressPlay() {
        levelName = Levels.Tutorial;
    }

    public void loadWaterLevel() {
        levelName = Levels.Level_Water;
        player = new PlayerController(false, 100f);
        score = 0;
    }
    
}
